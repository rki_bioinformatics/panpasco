import glob
import os

SNPCALLINGDIR = config.get("SNPcalling_dir", "SNP_calling")
OUTFILENAME = config["output_prefix"]
IDFILE= config["samples_list"]
GENOMEGAPSFILE = str(config["genomegaps_file"])
GENOMEFILE = config["genome_file"]
EXCLUDEREGIONS = config.get("exclude_regions_file", None)

SUMMARY_SCRIPT = srcdir("get_snp_summary.R")
DISTANCE_SCRIPT = srcdir("analyze_snp_summary_matrices_pairwise.R")


def check_exclude(wildcards):
    if EXCLUDEREGIONS:
        if not os.path.exists(EXCLUDEREGIONS):
            raise Exception('Missing input files for rule distance:\n{}'.format(EXCLUDEREGIONS))
        
        return ( " ".join([" --exclude_regions", EXCLUDEREGIONS]))
    
    return ("")


rule all:
     input: expand("distance/{outfilename}.RSave", outfilename=OUTFILENAME),
            expand("distance/{outfilename}_pairwise_rel_fract.txt", outfilename=OUTFILENAME),


rule fa_index:
    input: "{genome}"
    output: "{genome}.fai"
    shell: "samtools faidx {input}"



rule getinfo:
    input: 
         snp = SNPCALLINGDIR,
         ids = IDFILE,
         fai = expand("{genomefile}.fai",genomefile=GENOMEFILE),
         gaps = GENOMEGAPSFILE
    output: expand("distance/{outfilename}.RSave", outfilename=OUTFILENAME)
    shell: "Rscript {SUMMARY_SCRIPT} --input_dir {input.snp} --id_file {input.ids} --fastaidx_file {input.fai} --genome_gaps_file {input.gaps}  --output_file {output}"


rule distance:
    input:  dist=expand("distance/{outfilename}.RSave", outfilename=OUTFILENAME),
    output: expand("distance/{outfilename}_pairwise_rel_fract.txt", outfilename=OUTFILENAME),
            temp(expand("distance/{outfilename}_pairwise_count.txt", outfilename=OUTFILENAME))
    params: outfilename=OUTFILENAME, exclude=check_exclude
    shell: "Rscript {DISTANCE_SCRIPT} --input_data {input.dist} --output_dir distance --output_prefix {params.outfilename} {params.exclude}"

