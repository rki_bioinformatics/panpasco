# PANPASCO - PAN-genome based PAirwise Snp Comparison

Use pan-genome mapping and relative pairwise SNP distance calculation for transmission analyses.  


For contacting the developer and issue reports please go to https://gitlab.com/chrjan/panpasco/issues .  

Jandrasits C, Kröger S, Haas W, Renard BY.   
Computational pan-genome mapping and pairwise SNP-distance improve detection of Mycobacterium tuberculosis transmission clusters.  
PLoS Comput Biol. 2019;15(12):e1007527.  
Published 2019 Dec 9. [doi:10.1371/journal.pcbi.1007527](https://doi.org/10.1371/journal.pcbi.1007527)  
    
    
This workflow includes two steps:
* WGS alignment and variant calling
* SNP distance calculation

## Install via Bioconda

To install Conda please refer to the [Conda Installation Guide](https://conda.io/docs/user-guide/install/index.html) and add the [bioconda channel](http://ddocent.com//bioconda/).

### Install panpasco in a separate environment:
```
conda create -n panpasco panpasco
```

## RUN
To run the complete workflow do the following:
* Create an idfile for all your samples with one ID per line
* Copy all .fastq.gz files of your samples into one directory (ID_1.fastq.gz, ID_2.fastq.gz) 
* Activate the conda environment (full installation see above): 
```
conda activate panpasco
```
* Download the provided configfile templates (configfile.txt, configfile_distance.txt) and the pangenome folder from this repository.
* Change configfile.txt
    * 'samples_list' to /path/to/idfile 
    * genome_file to /path/to/pangenome/pangenomeMTb_consensus.fasta
    * fastqdir to /path/to/fastqdir   (default: "fastq" in the working directory)
* Run the alignment workflow:  
```
panpasco-pipeline --configfile configfile.txt
```
  The workflow will create several directories and files for the whole mapping and variant calling workflow.   
  The only folder that MUST be kept for distance analysis is "SNP_calling".   
  See below for a detailed description of the different files and steps and config parameters.   
  The provided `pangenome_genomegaps.bed` file is only valid in combination with the `pangenomeMTB_consensus.fasta` provided in this repository!   
   
* Change configfile_distance.txt
    * 'samples_list' property to /path/to/idfile
    * genome_file to /path/to/pangenome/pangenomeMTB_consensus.fasta   
    * genomegaps_dir to /path/to/pangenome/pangenome_genomegaps.bed
* Run the distance workflow to calculate distance matrices:   
```
panpasco-distance --configfile configfile_distance.txt
```


  Find your results of pairwise distance calculation in "analysis_pairwise_rel_fract.txt".   
  See below for a detailed description of the created files and script parameters.  


## Details
### Mapping and Variant Calling Pipeline

![Mapping, Variant Calling and Filtering Workflow](/images/workflow.png)*Mapping, Variant Calling and Filtering Workflow*
   
   
### Parameters

These parameters are set in the configuration files.

Alignment Workflow:

| Parameter | description | default |
| ------ | ------ | ----------|
| genome_file | Reference genome file | *pan-genome reference provided in this repository* |
| samples_list | File with list of samples with one ID per line| |
| fastqdir | Directory with .fastq.gz files named ID_1.fastq.gz, ID_2.fastq.gz| fastq |
| fastqpostfix | Specification of fastq.gz format; e.g. for the format sample_R1.fastq.gz put "fastqpostfix: R" | R |
| allele_frequency_threshold | Allele frequency treshold for definition of high-quality SNPs  | 0.75 |
| mapping_quality_threshold | Minimum Mapping Quality for reads to be used with GATK HaplotypeCaller | 10 |  
| depth_threshold | Minimum coverage depth for high-quality regions | 5 |
| flash_overlap | Number of overlapping basepairs of paired reads for FLASH to merge reads | 10 |
| trimmomatic_read_minimum_length | Minimum length of reads to be kept after trimming | 50 |
| trimmomatic_qual_slidingwindow | Minimum quality in sliding window in trimming | 15 |
| trimmomatic_adapter_file | File with adapters for trimming | *provided with installation* |


Distance Workflow:

| Parameter | description | default |
| ------ | ------ | ----------|
| genome_file | Reference genome file | *pan-genome reference provided in this repository* |
| samples_list | File with list of samples with one ID per line| |
| output_prefix | Prefix for all distance files | all_samples |
| genomegaps_file| File in .bed format with gaps in WGA of pan-genome| *provided in this repository* |
| SNPcalling_dir | Location of SNP calling results of alignment workflow | SNP_calling |
| exclude_regions_file | File in .bed format with positions that should be excluded from distance analysis | - |



### Details on dependencies

#### Dependencies
For running the Mapping and Variant Calling Pipeline the following programs are needed:
* GATK   (3.8)
* FastQC (v0.11.5)
* matplotlib (2.0)  
* PICARD (2.15.0)
* python (3.6.3)
* snakemake  (3.12)
* flash (v1.2.11)
* seqtk (1.0-r75)
* Trimmomatic (0.36)
* bwa (0.7.12)
* samtools  (1.3)
* tabix (0.2.5)
* bedtools (2.27.1)

Version 3.8 of GATK is needed, as some functionalities of GATK 3.8 were not ported to GATK4 yet.


For calculating the pairwise distance matrices the following programs are needed:
* R (3.2.2)
* GenomicRanges,  R-package on Bioconductor (1.26.4)
* argparse,  R-package  (1.0.1)
